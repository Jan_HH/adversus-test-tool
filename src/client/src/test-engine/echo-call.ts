import * as JsSIP from 'jssip'
import { RTCSessionEvent, UAConfiguration } from 'jssip/lib/UA'
import {
  RTCSession,
  SDPEvent,
  EndEvent,
  IncomingEvent
} from 'jssip/lib/RTCSession'
import { causes } from 'jssip/lib/Constants'
import * as TransformSdp from 'sdp-transform'
import * as Mos from '../lib/services/stream'
import { updateStatus } from '../redux/slices/test'
import { addEntry } from '../redux/slices/log'
import { setOpusMos, setPcmaMos } from '../redux/slices/result'

const HOST = 'ast23.adversus-server.dk'
const PORT = '8193'
const USER = 'jh'
const PW = 'd@l_r@cOSwA5O!u6R@24'

const testEcho = (vm: TestCase, dispatch: any, codec: Codec): Promise<void> => {
  return new Promise(resolve => {
    const { id, title } = vm
    dispatch(addEntry({ message: `Starting ${title}` }))
    dispatch(updateStatus({ id, status: 'Running' }))

    let session: RTCSession | null
    let recorder: MediaRecorder
    let stream: MediaStream
    const socket = new JsSIP.WebSocketInterface(`wss://${HOST}:${PORT}/ws`)
    const uaConfig: UAConfiguration = {
      sockets: [socket],
      uri: `sip:${USER}@${HOST}`,
      password: PW
    }
    const userAgent = new JsSIP.UA(uaConfig)
    const remoteAudio = new window.Audio()
    remoteAudio.autoplay = true
    let chunks: any = []

    const sessionAccepted = (event: IncomingEvent) => {
      dispatch(
        addEntry({
          message: 'Succesfully connected to service.',
          result: 'Success'
        })
      )
    }

    const sessionFailed = (event: EndEvent) => {
      dispatch(addEntry({ result: 'Error', message: `Error: ${event.cause}.` }))
      if (event.cause === causes.INCOMPATIBLE_SDP) {
        dispatch(
          addEntry({
            result: 'Error',
            message: `${codec} not supported at endpoint.`
          })
        )
      }
      dispatch(updateStatus({ id, status: 'Failed' }))
      userAgent.stop()
      resolve()
    }

    const recordEcho = () => {
      dispatch(addEntry({ message: 'Your turn to speak...' }))
      recorder.start()
      remoteAudio.srcObject = null
      Mos.getRTCPStats()
      window.setTimeout(endRecording, 5000)
    }

    const endRecording = () => {
      Mos.getRTCPStats()
      recorder.stop()
    }

    const sessionEnded = (event: EndEvent) => {
      const precision = 2
      const mos = Mos.getAverageScores().value
      const latency = Mos.getAverageScores().latency
      const jitter = Mos.getAverageScores().jitter
      const loss = Mos.getAverageScores().loss
      dispatch(
        addEntry({
          message: `Average scores:
          Latency: ${latency.toFixed(precision)}ms
          Jitter: ${jitter.toFixed(precision)} Δms
          Packet loss: ${loss.toFixed(precision)}%
          Resulting MoS: ${mos.toFixed(precision)}`,
          result: 'Success'
        })
      )
      codec === 'opus' ? dispatch(setOpusMos(mos)) : dispatch(setPcmaMos(mos))
      userAgent.stop()
      resolve()
    }

    const playbackRecording = () => {
      dispatch(addEntry({ message: 'What we heard.' }))
      const blob = new Blob(chunks)
      remoteAudio.src = URL.createObjectURL(blob)
      Mos.getRTCPStats()

      window.setTimeout(() => {
        dispatch(updateStatus({ id, status: 'Passed' }))
        userAgent.stop()
      }, 5000)
    }

    const forceCodec = (event: SDPEvent) => {
      let forcedCodec
      if (codec === 'opus') forcedCodec = '109'
      if (codec === 'pcma') forcedCodec = '8'
      if (event.originator === 'local') {
        const transformed = TransformSdp.parse(event.sdp)
        transformed.media[0].payloads = forcedCodec
        event.sdp = TransformSdp.write(transformed)
      }
    }

    const sessionConfirmed = (event: IncomingEvent) => {
      Mos.registerRTCPeerConnection(session!.connection)
      Mos.getRTCPStats()

      dispatch(addEntry({ message: 'Playing introduction.' }))
      if (session!.connection.getReceivers().length > 0) {
        stream = new MediaStream()
        recorder = new MediaRecorder(stream)
        session!.connection.getReceivers().forEach(receiver => {
          stream.addTrack(receiver.track)
        })
        remoteAudio.srcObject = stream
        Mos.getRTCPStats()
        recorder.ondataavailable = event => {
          chunks.push(event.data)
        }
        window.setTimeout(recordEcho, 22000)
        recorder.onstop = playbackRecording
      }
    }
    const handleNewRTC = (event: RTCSessionEvent) => {
      if (session) session.terminate()
      session = event.session
      session.on('sdp', forceCodec)
      session.on('ended', sessionEnded)
      session.on('failed', sessionFailed)
      session.on('accepted', sessionAccepted)
      session.on('confirmed', sessionConfirmed)
    }

    userAgent.on('newRTCSession', handleNewRTC)
    userAgent.on('disconnected', () => resolve())
    userAgent.start()
    userAgent.on('connected', () => {
      const options = {
        mediaConstraints: { audio: true, video: false }
      }
      userAgent.call('echo', options)
    })
  })
}

export default testEcho
