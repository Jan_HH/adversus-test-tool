import { addEntry } from '../redux/slices/log'
import { setTcpThroughput, setUdpThroughput } from '../redux/slices/result'
import { updateStatus } from '../redux/slices/test'

const testThroughput = (
  vm: TestCase,
  dispatch: any,
  protocol: Protocol
): Promise<void> => {
  return new Promise(resolve => {
    const { id, title } = vm

    let signaler: WebSocket
    let peer: RTCPeerConnection
    let datachannel: RTCDataChannel

    const bytesPerPayload = 66560
    const payload = new Array(bytesPerPayload).fill('X').join('')
    const thresholdHigh = bytesPerPayload * 12
    const thresholdLow = bytesPerPayload
    const megsToSend = 25 * 1024 * 1024
    const opusEstimates = 50
    const pcmaEstimates = 100
    let testStartTime: DOMHighResTimeStamp
    let timeout: NodeJS.Timeout | null
    let highestThroughput: number
    let lowestThroughput: number
    let currentThroughput = 0
    let received = 0

    const send = (): void => {
      if (timeout !== null) {
        clearTimeout(timeout)
        timeout = null
      }
      let bufferedAmount = datachannel.bufferedAmount
      while (received < megsToSend) {
        datachannel.send(payload)
        bufferedAmount += bytesPerPayload

        if (bufferedAmount >= thresholdHigh) {
          if (datachannel.bufferedAmount < thresholdLow) {
            timeout = setTimeout(() => send(), 0)
          }
          return
        }
      }
    }

    const startTransmission = () => {
      dispatch(addEntry({ message: `Starting ${title}` }))
      dispatch(updateStatus({ id, status: 'Running' }))
      datachannel.binaryType = 'arraybuffer'
      testStartTime = performance.now()
      send()
    }

    const receiveTranmission = (message: MessageEvent) => {
      received += message.data.length
      currentThroughput = received / (performance.now() - testStartTime)
      if (!highestThroughput || currentThroughput > highestThroughput)
        highestThroughput = currentThroughput
      if (!lowestThroughput || currentThroughput < lowestThroughput)
        lowestThroughput = currentThroughput
      if (received >= megsToSend) done()
    }

    const bufferedAmountLow = () => {
      protocol === 'udp'
        ? dispatch(setUdpThroughput(currentThroughput))
        : dispatch(setTcpThroughput(currentThroughput))
      send()
    }

    const rtcHandshake = () => {
      peer.createOffer().then((sdp: RTCSessionDescriptionInit) => {
        peer.setLocalDescription(sdp).then(() => {
          signaler.send(JSON.stringify({ signal: 'SDP_OFFER', payload: sdp }))
        })
      })

      signaler.onmessage = (message: MessageEvent) => {
        const parsed = JSON.parse(message.data)
        if (parsed.signal === 'SDP_ANSWER')
          peer.setRemoteDescription(parsed.payload)

        if (parsed.signal === 'ICE_CANDIDATE')
          peer.addIceCandidate(parsed.payload)
      }
      peer.onicecandidate = (event: RTCPeerConnectionIceEvent) => {
        const candidate = event.candidate
        if (!candidate || !candidate.candidate.includes(protocol)) return
        signaler.send(
          JSON.stringify({ signal: 'ICE_CANDIDATE', payload: candidate })
        )
      }
    }

    const done = (): void => {
      const averageThroughput = received / (performance.now() - testStartTime)
      dispatch(
        addEntry({
          message: `Highest transfer rate: ${highestThroughput.toFixed(2)}`
        })
      )
      dispatch(
        addEntry({
          message: `Lowest transfer rate: ${lowestThroughput.toFixed(2)}`
        })
      )
      dispatch(
        addEntry({
          message: `Average transfer rate: ${averageThroughput.toFixed(2)}`
        })
      )
      dispatch(
        addEntry({
          message: `Estimated simultaneous connection supported: 
        OPUS (estimated 50 kBit/s per connection): 
          Highest: ${(highestThroughput / opusEstimates).toFixed(0)}
          Lowest: ${(lowestThroughput / opusEstimates).toFixed(0)}
          Average: ${(averageThroughput / opusEstimates).toFixed(0)}
        PCMA (estimated 100 kBit/s per connection): 
          Highest: ${(highestThroughput / pcmaEstimates).toFixed(0)}
          Lowest: ${(lowestThroughput / pcmaEstimates).toFixed(0)}
          Average: ${(averageThroughput / pcmaEstimates).toFixed(0)}`,
          result: 'Success'
        })
      )
      dispatch(updateStatus({ id, status: 'Passed' }))
      peer.close()
      signaler.close()
    }

    const cleanUp = (): void => {
      resolve()
    }

    signaler = new WebSocket(`ws://localhost:5000/${protocol}`)
    peer = new RTCPeerConnection()

    signaler.onclose = cleanUp
    peer.onconnectionstatechange = cleanUp

    datachannel = peer.createDataChannel('throughput', { ordered: false })
    datachannel.onopen = startTransmission
    datachannel.onbufferedamountlow = bufferedAmountLow
    datachannel.onmessage = receiveTranmission

    signaler.onopen = rtcHandshake
  })
}

export default testThroughput
