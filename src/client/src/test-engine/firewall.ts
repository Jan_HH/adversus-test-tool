import * as JsSIP from 'jssip'
import { UAConfiguration } from 'jssip/lib/UA'
import { addEntry } from '../redux/slices/log'
import { updateStatus } from '../redux/slices/test'

const HOST = 'ast23.adversus-server.dk'
const PORT = '8193'
const USER = 'jh'
const PW = 'd@l_r@cOSwA5O!u6R@24'

const testFirewall = (vm: TestCase, dispatch: any): Promise<void> => {
  return new Promise(resolve => {
    const { id, title } = vm
    dispatch(updateStatus({ id, status: 'Running' }))
    dispatch(addEntry({ message: `Starting ${title}` }))
    dispatch(
      addEntry({ message: 'Waiting for signal from service endpoint...' })
    )

    const registrationSucceded = (event: any) => {
      dispatch(addEntry({ result: 'Success', message: 'Signal received!' }))
      dispatch(updateStatus({ id, status: 'Passed' }))
      userAgent.stop()
    }
    const registrationFailed = (event: any) => {
      dispatch(
        addEntry({ result: 'Error', message: 'Service was not reached.' })
      )
      dispatch(updateStatus({ id, status: 'Failed' }))
      userAgent.stop()
    }
    const unregistrationSucceded = (event: any) => {
      socket.disconnect()
      resolve()
    }

    const socket = new JsSIP.WebSocketInterface(`wss://${HOST}:${PORT}/ws`)
    const config: UAConfiguration = {
      sockets: [socket],
      uri: `sip:${USER}@${HOST}`,
      password: PW
    }
    const userAgent = new JsSIP.UA(config)

    userAgent.on('registered', registrationSucceded)
    userAgent.on('registrationFailed', registrationFailed)
    userAgent.on('unregistered', unregistrationSucceded)
    userAgent.start()
  })
}

export default testFirewall
