import { start } from '../redux/slices/audio-visualiser'
import { addEntry } from '../redux/slices/log'
import { updateStatus } from '../redux/slices/test'

const testMicrophone = (vm: TestCase, dispatch: any): Promise<void> => {
  return new Promise(async resolve => {
    const { id, title } = vm
    dispatch(addEntry({ message: `Starting ${title}` }))
    dispatch(updateStatus({ id, status: 'Running' }))
    try {
      const stream = await window.navigator.mediaDevices.getUserMedia({
        audio: true
      })
      const devices = await window.navigator.mediaDevices.enumerateDevices()
      dispatch(start({ stream, devices }))
      dispatch(addEntry({ result: 'Success', message: 'Microphone found!' }))
      dispatch(updateStatus({ id, status: 'Passed' }))
      resolve()
    } catch (error) {
      dispatch(addEntry({ result: 'Error', message: `${error}` }))
      dispatch(updateStatus({ id, status: 'Failed' }))
    }
  })
}

export default testMicrophone
