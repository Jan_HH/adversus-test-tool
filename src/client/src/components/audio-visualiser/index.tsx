import { FC, useEffect, useRef } from 'react'
import { useAppSelector } from '../../redux/hooks'
import './style.scss'

const AudioVisualizer: FC = (props: any) => {
  const { analyser } = useAppSelector(state => state.audiovisualiserReducer)
  const canvasRef = useRef<HTMLCanvasElement>(null)

  useEffect(() => {
    if (!analyser) {
      return
    }

    let frame: number
    analyser.fftSize = 256
    const data = new Uint8Array(analyser.frequencyBinCount)
    const canvas = canvasRef.current
    analyser.minDecibels = -90
    analyser.maxDecibels = -10
    analyser.smoothingTimeConstant = 0.9

    const render = () => {
      frame = requestAnimationFrame(render)
      analyser.getByteFrequencyData(data)
      if (canvas) {
        const { height, width } = canvas
        const context = canvas.getContext('2d')
        let x = 0
        if (context) {
          analyser.getByteFrequencyData(data)

          let gradient = context.createLinearGradient(0, height, 0, 0)
          gradient.addColorStop(0, 'green')
          gradient.addColorStop(0.5, 'yellow')
          gradient.addColorStop(1, 'red')

          context.fillStyle = 'white'
          context.fillRect(0, 0, width, height)

          let barWidth = width / data.length
          let barHeight

          for (const amplitude of data) {
            barHeight = amplitude

            context.fillStyle = gradient
            context.fillRect(x, height - barHeight, barWidth, barHeight)

            x += barWidth + 1
          }
        }
      }
    }
    render()

    return () => {
      cancelAnimationFrame(frame)
    }
  }, [canvasRef, analyser])

  return <canvas ref={canvasRef} className='visualiser' />
}

export default AudioVisualizer
