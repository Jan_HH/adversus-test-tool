import { useAppSelector } from '../../redux/hooks'
import Gauge from '../gauge'

const ThroughputTest = (props: any) => {
  const { throughputUdp, throughputTcp } = useAppSelector(
    state => state.resultReducer
  )

  const { title } = props

  return (
    <>
      {title.includes('UDP') ? (
        <Gauge title={'AVERAGE'} value={throughputUdp.toFixed(2)} />
      ) : (
        <Gauge title={'AVERAGE'} value={throughputTcp.toFixed(2)} />
      )}
    </>
  )
}

export default ThroughputTest
