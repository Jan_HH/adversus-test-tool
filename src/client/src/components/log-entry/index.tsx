import './style.scss'

const LogEntry = (props: any) => {
  const { message, result } = props

  return (
    <div className='log__entry__wrapper'>
      <div className={`log__entry${result ? '--' + result.toLowerCase() : ''}`}>
        {message}
      </div>
    </div>
  )
}

export default LogEntry
