import './style.scss'

const Test = (props: any) => {
  const { status, title, description, executing } = props

  return (
    <div className='test__container panel'>
      <div className='raised'>
        <div className='test__title'>
          <div className={`test__status${'--' + status.toLowerCase()}`}>
            {status}
          </div>
          <div>{title}</div>
        </div>
        <div className='test__description'>{description}</div>
      </div>
      {status !== 'Pending' && executing ? (
        <div className='test__body'>{executing}</div>
      ) : null}
    </div>
  )
}

export default Test
