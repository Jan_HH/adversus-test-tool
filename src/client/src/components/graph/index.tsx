import React from 'react'
import './style.scss'

const Graph = (props: any) => {
  const upGraphRef = React.useRef<HTMLCanvasElement>(null)
  const downGraphRef = React.useRef<HTMLCanvasElement>(null)
  let x = 0
  let y = 180

  const draw = (context: any, frameCount: any) => {
    context.clearRect(0, 0, context.canvas.width, context.canvas.height)
    context.beginPath()
    context.moveTo(frameCount, y)
  }

  React.useEffect(() => {
    const upGraphContext = upGraphRef.current!.getContext('2d')
    const downGraphContext = downGraphRef.current!.getContext('2d')
    let frameCount = 0
    initContext(upGraphContext, '#ff0000')
    initContext(downGraphContext, '#0000ff')
    upGraphContext!.moveTo(120, 0)
    downGraphContext!.moveTo(180, 0)

    const render = () => {
      frameCount++
      frameCount = frameCount % 300
      draw(upGraphContext, frameCount)
      draw(downGraphContext, frameCount)
      window.requestAnimationFrame(render)
    }
    render()
  })

  return (
    <div>
      <canvas ref={upGraphRef}></canvas>
      <canvas ref={downGraphRef}></canvas>
    </div>
  )

  function initContext(
    context: CanvasRenderingContext2D | null,
    lineColor: string
  ) {
    context!.lineWidth = 2
    context!.strokeStyle = lineColor
  }
}

export default Graph
