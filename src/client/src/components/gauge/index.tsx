import './style.scss'

const Gauge = (props: any) => {
  const { title, value } = props

  return (
    <div className='gauge__container'>
      <div className='gauge__title'>{title}</div>
      <div className='gauge__display'>
        <div className='gauge__value'>{value}</div>
        <div className='gauge__unit'>kBit/s</div>
      </div>
    </div>
  )
}

export default Gauge
