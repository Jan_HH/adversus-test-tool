import { useAppSelector } from '../../redux/hooks'
import LogEntry from '../log-entry'

const Log = () => {
  const entries = useAppSelector(state => state.logReducer)

  return (
    <>
      {entries.map((entry: LogEntry) => (
        <LogEntry
          key={entry.id}
          result={entry.result}
          message={entry.message}
        />
      ))}
    </>
  )
}

export default Log
