import { useAppSelector } from '../../redux/hooks'
import Test from '../test'

const TestSuite = () => {
  const tests = useAppSelector(state => state.testReducer)

  return (
    <>
      {tests.map((test: TestCase) => (
        <Test
          key={test.id}
          status={test.status}
          title={test.title}
          description={test.description}
          executing={
            test.executing ? <test.executing title={test.title} /> : null
          }
        />
      ))}
    </>
  )
}

export default TestSuite
