import { useRef } from 'react'
import { useAppSelector } from '../../redux/hooks'
import { changeDevice } from '../../redux/slices/audio-visualiser'
import AudioVisualiser from '../audio-visualiser'
import './style.scss'

const MicrophoneTest = () => {
  const { devices, stream } = useAppSelector(
    state => state.audiovisualiserReducer
  )
  const selectRef = useRef<HTMLSelectElement>(null)

  const deviceChanged = async (elem: HTMLSelectElement) => {
    const stream = await window.navigator.mediaDevices.getUserMedia({
      audio: { deviceId: elem.value }
    })
    stream.getAudioTracks().forEach(track => console.log(track))
    changeDevice(stream)
  }
  const savePreference = () => {}

  return (
    <>
      <div className='mic__preference__wrapper'>
        {devices && devices.length > 0 ? (
          <>
            <select
              onChange={event => deviceChanged(event.target)}
              ref={selectRef}>
              {devices.map((mic: MediaDeviceInfo) => (
                <option key={mic.deviceId} value={mic.deviceId}>
                  {mic.label}
                </option>
              ))}
            </select>
            <button onClick={savePreference}>Pick</button>
          </>
        ) : null}
      </div>
      {stream ? <AudioVisualiser /> : null}
    </>
  )
}

export default MicrophoneTest
