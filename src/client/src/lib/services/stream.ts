import * as Mos from "./mos"
export * from './mos'

let _pc: RTCPeerConnection | null = null

export function registerRTCPeerConnection(pc: RTCPeerConnection) {
	_pc = pc
}
export function onRemoveRTCPeerConnection() {
	_pc = null
}

export async function getRTCPStats(): Promise<PeerConnectionInfo> {
	if (_pc === null) {
		return Promise.reject()
	}
	const rtp = ['inbound-rtp', 'outbound-rtp', 'remote-inbound-rtp', 'remote-outbound-rtp']
	const peerStats = await _pc.getStats(null)
	const entries = Array.from(peerStats['values']())
		.filter(e => rtp.includes(e['type']))
	const stats = {
		sctp: _pc.sctp && _pc.sctp.state || null,
		state: _pc.connectionState,
		entries
	};

	let current: Partial<RawConnectionInfo> = {
		local: {},
		remote: {}
	};

	for (const entry of entries) {
		current = {
			...current,
			...transformRTCPStatEntry(current, entry)
		}
	}

	Mos.onUpdateRawConnectionInfo(current as RawConnectionInfo)

	return {
		...stats,
		...current,
	} as PeerConnectionInfo
}

function transformRTCPStatEntry(stats: Partial<PeerConnectionInfo>, entry: any) {
	switch (entry.type) {
		case 'inbound-rtp':
			return {
				local: {
					...stats.local,
					...transformInboundRTCPStatEntry(entry),
					inboundNackCount: entry['nackCount'],
					jitter: entry['jitter']
				}
			}
		case 'outbound-rtp':
			return {
				local: {
					...stats.local,
					packetsSent: entry['packetsSent'],
					outboundNackCount: entry['nackCount']
				}
			}
		case 'remote-inbound-rtp':
			return {
				remote: {
					...stats.remote,
					...transformInboundRTCPStatEntry(entry),
					rtt: entry['roundTripTime']
				}
			}
		case 'remote-outbound-rtp':
			return {
				remote: {
					...stats.remote,
					packetsSent: entry['packetsSent']
				}
			}
	}
	return {}
}

function transformInboundRTCPStatEntry(entry: any) {
	return {
		packetsLost: entry['packetsLost'],
		packetsReceived: entry['packetsReceived'],
		jitter: entry['jitter']
	}
}
