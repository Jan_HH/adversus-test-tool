interface RawConnectionInfo {
	local: Partial<{
		packetsLost: number;
		packetsReceived: number;
		packetsSent: number;
		inboundNackCount: number;
		outboundNackCount: number;
		jitter: number;
	}>;
	remote: Partial<{
		packetsLost: number;
		packetsReceived: number;
		packetsSent: number;
		jitter: number;
		rtt: number;
	}>;
}

interface PeerConnectionInfo extends RawConnectionInfo {
	state: RTCPeerConnectionState;
	sctp: RTCSctpTransportState | null;
	entries: unknown[];
}

interface QualityRecord {
	value: number;
	jitter: number;
	latency: number;
	loss: number;
}