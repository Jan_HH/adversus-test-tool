let audioScoresLog: QualityRecord[] = []
let statsLog: RawConnectionInfo[] = []
const SCORES_LOG_LENGTH = 10

export function onUpdateRawConnectionInfo(current: RawConnectionInfo) {
	statsLog.push(current)
	if (statsLog.length < 2) {
		return
	}
	const audioScore = calculateAudioQuality(statsLog)
	audioScoresLog.push(audioScore)

	while (statsLog.length > SCORES_LOG_LENGTH) {
		statsLog.shift()
	}
	while (audioScoresLog.length > SCORES_LOG_LENGTH) {
		audioScoresLog.shift()
	}
}

export function reset() {
	audioScoresLog = []
	statsLog = []
}

export function getAverageScores() {
	let sum: QualityRecord = {
		value: 0,
		latency: 0,
		jitter: 0,
		loss: 0
	}
	for (let i = 0; i < audioScoresLog.length; i++) {
		const score = audioScoresLog[i]
		sum = {
			value: score.value + sum.value,
			latency: score.latency + sum.latency,
			jitter: score.jitter + sum.jitter,
			loss: score.loss + sum.loss
		}
	}
	return {
		value: sum.value / audioScoresLog.length,
		latency: sum.latency / audioScoresLog.length,
		jitter: sum.jitter / audioScoresLog.length,
		loss: sum.loss / audioScoresLog.length
	}
}


function calculateAudioQuality(stats: RawConnectionInfo[]): QualityRecord {
	const audioScore = function (rtt: number, jitter: number, plr: number): number {
		const LOCAL_DELAY = 20 // 20 msecs: typical frame duration
		function H(x: number) {
			return (x < 0 ? 0 : 1)
		}
		const a = 0 // ILBC: a = 10
		const b = 19.8
		const c = 29.7

		//R = 94.2 − Id − Ie
		const R = function (rtt: number, jitter: number, packetLoss: number) {
			const d = rtt + LOCAL_DELAY
			const Id = 0.024 * d + 0.11 * (d - 177.3) * H(d - 177.3)

			const P = packetLoss
			const Ie = a + b * Math.log(1 + c * P)

			const R = 94.2 - Id - Ie - 0.6 * 100 * ((jitter / 3) * 100)

			return R
		}

		//For R < 0: MOS = 1
		//For 0 R 100: MOS = 1 + 0.035 R + 7.10E-6 R(R-60)(100-R)
		//For R > 100: MOS = 4.5
		const MOS = function (R: number) {
			if (R < 0) {
				return 1
			}
			if (R > 100) {
				return 4.5
			}
			return 1 + 0.035 * R + 7.10 / 1000000 * R * (R - 60) * (100 - R)
		}

		return MOS(R(rtt, jitter, plr))
	}

	if (stats.length < 2) {
		return {
			value: 0,
			jitter: 0,
			latency: 0,
			loss: 0
		};
	}
	const currentStats = stats[stats.length - 1]
	const lastStats = stats[stats.length - 2]

	const totalAudioPackets =
		(currentStats.local.packetsLost! - lastStats.local.packetsLost!) +
		(currentStats.local.packetsReceived! - lastStats.local.packetsReceived!)
	if (0 === totalAudioPackets) {
		return {
			value: 0,
			jitter: 0,
			latency: 0,
			loss: 0
		}
	}
	const plr = (currentStats.local.packetsLost! - lastStats.local.packetsLost!) /
		totalAudioPackets
	const jitter = currentStats.local.jitter!
	const rtt = currentStats.remote.rtt || 0

	return {
		value: audioScore(rtt, jitter, plr),
		jitter: jitter * 1000,
		latency: rtt * 1000,
		loss: plr
	}
}
