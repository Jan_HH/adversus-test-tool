export const colors = {
	// Contrast
	black: "#181C2F",
	white: "#FFFFFF",
	shadow: "#32325d",

	// Grayscale
	gray300: "#FAFAFB",
	gray400: "#EBECEF",
	gray500: "#E0E2E6",
	gray600: "#D6D9DE",
	gray700: "#A9ABB2",
	gray800: "#72767F",

	// System
	link: "#356CFA",
	error: "#FF4D35",
	warning: "#FFBC27",
	success: "#00B67A",

	// Brand
	brandPrimary: "#3639A4",
	brandSecondary: "#75E8F0",
	secondary: "#FFB0A3",
	cta: "#FF7364",
}

export const theme = {
	colors
}
