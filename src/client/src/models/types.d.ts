interface TestCase {
  id: number
  status: 'Pending' | 'Running' | 'Passed' | 'Failed'
  title: string
  description: string
  executing?: any
}

interface LogEntry {
  id?: number
  result?: 'Success' | 'Error'
  message: string
}

type Codec = 'opus' | 'pcma'

type Protocol = 'udp' | 'tcp'

interface TestRunResult {
  userLatitude: number
  userLongitude: number
  mosOpus: number
  mosPcma: number
  throughputUdp: number
  throughputTcp: number
}
