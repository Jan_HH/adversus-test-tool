import Logo from './adversus-logo.svg'
import * as api from './api/database'
import './App.scss'
import Log from './components/log'
import TestSuite from './components/test-suite'
import { useAppDispatch, useAppSelector } from './redux/hooks'
import store from './redux/store'
import echo from './test-engine/echo-call'
import wall from './test-engine/firewall'
import mic from './test-engine/microphone'
import band from './test-engine/throughput'

const App = () => {
  const dispatch = useAppDispatch()
  const tests = useAppSelector(state => state.testReducer)

  const runSuite = async () => {
    try {
      await mic(tests[0], dispatch)
      await wall(tests[1], dispatch)
      // await echo(tests[2], dispatch, 'opus')
      // await echo(tests[3], dispatch, 'pcma')
      // await band(tests[4], dispatch, 'udp')
      // await band(tests[5], dispatch, 'tcp')
      // await saveResult()
    } catch (error) {
      console.error(error)
    }
  }

  const saveResult = async () => {
    navigator.geolocation.getCurrentPosition(loc => {
      api.storeResult({
        userLatitude: loc.coords.latitude,
        userLongitude: loc.coords.longitude,
        mosOpus: store.getState().resultReducer.mosOpus,
        mosPcma: store.getState().resultReducer.mosPcma,
        throughputUdp: store.getState().resultReducer.throughputUdp,
        throughputTcp: store.getState().resultReducer.throughputTcp
      })
    })
  }

  return (
    <main className='app'>
      <div className='logo'>
        <img src={Logo}></img>
        <span>connect</span>
      </div>
      <div className='instrument__headers'>
        <div className='testsuite__header'>
          <i className='fa fa-vial header__icon'></i>
          <span>Test Suite</span>
        </div>
        <button className={''} onClick={runSuite}>
          Run
        </button>
        <div className='log__header'>
          <span>Test Log</span>
          <i className='fa fa-pen header__icon'></i>
        </div>
      </div>
      <div className='instruments'>
        <div className='test__suite'>
          <TestSuite />
        </div>
        <div className='log__entries'>
          <Log />
        </div>
      </div>
    </main>
  )
}

export default App
