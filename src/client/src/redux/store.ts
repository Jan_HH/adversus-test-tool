import {
  Action,
  combineReducers, configureStore,
  ThunkAction
} from '@reduxjs/toolkit'
import * as api from '../api/database'
import audiovisualiserReducer from './slices/audio-visualiser'
import logReducer from './slices/log'
import resultReducer from './slices/result'
import testReducer from './slices/test'

const rootReducer = combineReducers({
  logReducer,
  testReducer,
  audiovisualiserReducer,
  resultReducer
})

const store = configureStore({
  reducer: rootReducer,
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware({
      thunk: {
        extraArgument: api
      },
      serializableCheck: false
    })
})

export type AppDispatch = typeof store.dispatch
export type RootState = ReturnType<typeof store.getState>
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>

export default store
