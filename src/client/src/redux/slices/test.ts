import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import EchoTest from '../../components/echocall-test'
import FirewallTest from '../../components/firewall-test'
import MicrophoneTest from '../../components/microphone-test'
import ThroughputTest from '../../components/throughput-test'

const initialState: TestCase[] = [
  {
    id: 0,
    status: 'Pending',
    title: 'Test: Microphone',
    description: 'Check if the system has an accessible microphone.',
    executing: MicrophoneTest
  },
  {
    id: 1,
    status: 'Pending',
    title: 'Test: Firewall',
    description: 'Sends a message to the service, and expects a reply.',
    executing: null
  },
  {
    id: 2,
    status: 'Pending',
    title: 'Test: Echo call - Opus',
    description:
      'Performs a call. Confirms that the service accepts Opus encoded audio, and that it can be successfully transmitted.',
    executing: null
  },
  {
    id: 3,
    status: 'Pending',
    title: 'Test: Echo call - PCMA',
    description:
      'Performs a call. Confirms that the service accepts PCMA encoded audio, and that it can be successfully transmitted.',
    executing: null
  },
  {
    id: 4,
    status: 'Pending',
    title: 'Test: Throughput - UDP',
    description: 'Attempts to gauge the bandwidth of the connection.',
    executing: ThroughputTest
  },
  {
    id: 5,
    status: 'Pending',
    title: 'Test: Throughput - TCP',
    description: 'Attempts to gauge the bandwidth of the connection.',
    executing: ThroughputTest
  }
]

const testSlice = createSlice({
  name: 'test',
  initialState,
  reducers: {
    updateStatus(
      state: TestCase[],
      { payload }: PayloadAction<{ id: number; status: any }>
    ) {
      state.find(test => test.id === payload.id)!.status = payload.status
    }
  }
})

export const { updateStatus } = testSlice.actions
export default testSlice.reducer
