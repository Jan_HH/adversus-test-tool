import { createSlice } from '@reduxjs/toolkit'

const initialState: TestRunResult = {
  userLatitude: 0,
  userLongitude: 0,
  mosOpus: 0,
  mosPcma: 0,
  throughputUdp: 0,
  throughputTcp: 0
}

const resultSlice = createSlice({
  name: 'result',
  initialState,
  reducers: {
    setOpusMos(state, action) {
      return { ...state, mosOpus: action.payload }
    },
    setPcmaMos(state, action) {
      return { ...state, mosPcma: action.payload }
    },
    setUdpThroughput(state, action) {
      return { ...state, throughputUdp: action.payload }
    },
    setTcpThroughput(state, action) {
      return { ...state, throughputTcp: action.payload }
    },
  }
})

export const { setOpusMos, setPcmaMos, setUdpThroughput, setTcpThroughput } =
  resultSlice.actions
export default resultSlice.reducer
