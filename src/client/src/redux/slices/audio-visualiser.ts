import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit'

interface Visualiser {
  analyser: AnalyserNode | null
  context: AudioContext | null
  source: MediaStreamAudioSourceNode | null
  stream: MediaStream | null
  devices: MediaDeviceInfo[]
}

const initialState: Visualiser = {
  analyser: null,
  context: null,
  source: null,
  stream: null,
  devices: []
}

const stopAnalyser = createAsyncThunk(
  'analyser/stopAnalyser',
  async (state: Visualiser) => {
    try {
      if (state.context) {
        await state.context.close()
        state.context = null
      }
      if (state.source) {
        state.source.disconnect()
        state.source = null
      }
      if (state.stream) {
        state.stream.getTracks().forEach(track => track.stop())
        state.stream = null
      }
      if (state.analyser) {
        state.analyser.disconnect()
        state.analyser = null
      }
    } catch (err) {
      console.error(err.name, err.message)
    }
    return state
  }
)

const audiovisualiserSlice = createSlice({
  name: 'analyser',
  initialState,
  reducers: {
    start(
      state: Visualiser,
      {
        payload
      }: PayloadAction<{ stream: MediaStream; devices: MediaDeviceInfo[] }>
    ) {
      state.stream = payload.stream
      state.devices = payload.devices
      state.context = new AudioContext()
      state.source = state.context.createMediaStreamSource(state.stream!)
      state.analyser = state.source.context.createAnalyser()
      state.analyser.smoothingTimeConstant = 1
      state.source.connect(state.analyser)
    },
    changeDevice(state: Visualiser, { payload }: PayloadAction<MediaStream>) {
      state.stream = payload
      state.context = new AudioContext()
      state.source = state.context.createMediaStreamSource(state.stream!)
      state.analyser = state.source.context.createAnalyser()
      state.analyser.smoothingTimeConstant = 1
      state.source.connect(state.analyser)
    }
  },
  extraReducers: builder => {
    builder.addCase(stopAnalyser.fulfilled, (state, action) => {
      state = action.payload
    })
  }
})

export const { start, changeDevice } = audiovisualiserSlice.actions
export default audiovisualiserSlice.reducer
