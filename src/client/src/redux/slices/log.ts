import { createSlice, PayloadAction } from '@reduxjs/toolkit'

const initialState: LogEntry[] = []

const logSlice = createSlice({
  name: 'log',
  initialState,
  reducers: {
    addEntry(state, { payload }: PayloadAction<LogEntry>) {
      state.push({ id: state.length, ...payload })
    }
  }
})

export const { addEntry } = logSlice.actions
export default logSlice.reducer
