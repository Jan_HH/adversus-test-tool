export const storeResult = async (result: TestRunResult) => {
  const url = 'http://localhost:5000/api/result/save'
   const response = await fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(result)
  });
  return response;
}