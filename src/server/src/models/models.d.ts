interface TestRunResult {
  user_latitude: number
  user_longitude: number
  mos_opus: number
  mos_pcma: number
  throughput_udp: number
  throughput_tcp: number
}
