import { Request } from 'express'
import { Socket } from 'net'
import {
  MessageEvent,
  RTCDataChannelEvent,
  RTCPeerConnection,
  RTCDataChannel,
  RTCPeerConnectionIceEvent,
  RTCSessionDescription
} from 'wrtc'
import WebSocket from 'ws'

let signaler: WebSocket
let peer: RTCPeerConnection
let dataChannel: RTCDataChannel
let protocol: string

const dataChannelCreated = (event: RTCDataChannelEvent) => {
  dataChannel = event.channel
  dataChannel.addEventListener('message', relayData)
  dataChannel.addEventListener('close', cleanUp)
}

const relayData = (data: MessageEvent) => {
  dataChannel.send(data.data)
}

const cleanUp = () => {
  peer.close()
  signaler.close()
}

const rtcHandshake = (message: MessageEvent) => {
  const parsed = JSON.parse(message.data)

  if (parsed.signal === 'SDP_OFFER') {
    peer.setRemoteDescription(parsed.payload)
    peer.createAnswer().then((sdp: RTCSessionDescription) => {
      peer.setLocalDescription(sdp)
      signaler.send(JSON.stringify({ signal: 'SDP_ANSWER', payload: sdp }))
    })
  }

  if (parsed.signal === 'ICE_CANDIDATE') {
    peer.addIceCandidate(parsed.payload)
  }

  peer.onicecandidate = (event: RTCPeerConnectionIceEvent) => {
    const candidate = event.candidate
    if (candidate && candidate.candidate.includes(protocol))
      signaler.send(
        JSON.stringify({ signal: 'ICE_CANDIDATE', payload: candidate })
      )
  }
}

const connectSocket = (websocket: WebSocket) => {
  signaler = websocket
  signaler.onmessage = rtcHandshake
  peer = new RTCPeerConnection()
  peer.ondatachannel = dataChannelCreated
}

const webSocketServer = new WebSocket.Server({ noServer: true })
webSocketServer.on('connection', connectSocket)

const WebRTC = (request: Request, socket: Socket, head: Buffer) => {
  switch (request.url) {
    case '/tcp':
      protocol = 'tcp'
      break

    case '/udp':
      protocol = 'udp'
      break

    default:
      socket.destroy()
  }
  webSocketServer.handleUpgrade(request, socket, head, (socket: WebSocket) => {
    webSocketServer.emit('connection', socket, request)
  })
}

export default WebRTC
