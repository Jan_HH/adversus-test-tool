import Mysql, { ResultSetHeader } from 'mysql2'

const db = Mysql.createConnection({
  socketPath: '/run/mysqld/mysqld.sock',
  user: 'jh',
  password: '1234',
  database: 'adversus_test'
})

interface TestRunResult {
  userLatitude: number
  userLongitude: number
  mosOpus: number
  mosPcma: number
  throughputUdp: number
  throughputTcp: number
}

export const insertTestRunResult = (
  result: TestRunResult
): Promise<ResultSetHeader> => {
  return new Promise((resolve, reject) => {
    const {
      userLatitude,
      userLongitude,
      mosOpus,
      mosPcma,
      throughputUdp,
      throughputTcp
    } = result

    db.execute(
      `INSERT INTO test_results 
      (
        user_latitude, 
        user_longitude, 
        mos_opus, 
        mos_pcma, 
        throughput_udp, 
        throughput_tcp
      )
      VALUES
      (
        ?, ?, ?, ?, ?, ?
      )`,
      [
        userLatitude,
        userLongitude,
        mosOpus,
        mosPcma,
        throughputUdp,
        throughputTcp
      ],
      (error, result) => {
        if (error) reject(error)
        resolve(result as ResultSetHeader)
      }
    )
  })
}
