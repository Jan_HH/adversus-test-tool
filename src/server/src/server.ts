import Express from 'express'
import Http from 'http'
import WebRTC from './controllers/webrtc'
import { insertTestRunResult } from './db'

const app = Express()
const server = Http.createServer(app)
const port = 5000

app.use(Express.json())
app.use(Express.urlencoded())

app.post('/api/result/save', (req, res) => {
  insertTestRunResult(req.body)
    .then(result => res.status(200).json({ result: result.insertId }))
    .catch(console.log)
})

server.on('upgrade', WebRTC)

server.listen(port, () =>
  console.log('Backend started, and listening on port', port)
)
